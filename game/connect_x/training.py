import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time
import torch
import torch.nn as nn
from typing import List
from agent import Agent
from env import ConnectXEnv
from replay import Transition
from tqdm import tqdm
from os.path import join
from pathlib import Path
from utils import moving_average


def train(
        agent: Agent,
        env: ConnectXEnv,
        checkpoints_dir_path: str,
        device: torch.device,
        episodes: int=50000,
        evaluation_episodes: int=100,
        batch_size: int=128,
        eval_period: int=25,
        enforce_valid_action: bool=True,
        checkpoint_every: int=10000,
        save_on_exit: bool=True,
        display_period: int=1000,

) -> tuple[pd.DataFrame, pd.DataFrame, list, str]:
    """
    Trains a policy on the Connect Four task. Loss is computed at every episode once
    the buffer has more transitions than the batch size value.

    Args:
        - `agent`: agent of type `Agent`.
        - `env`: environment of type `ConnectEnv`.
        - `checkpoints_dir_path`: path to the checkpoints' directory.
        - `device`: torch device.
        - `episodes`: number of episodes for training.
        - `evaluation_episodes`: number of episodes for evaluation.
        - `batch_size`: the batch_size used for training.
        - `eval_period`: period for the evaluation.
        - `enforce_valid_action`: whether to enforce that the action is valid or not.
        - `checkpoint_every`: frequency of checkpoint saves.
        - `save_on_exit`: save model at the end on training.


    Returns:
        - Pandas dataframe with the history of the training.
        - Pandas dataframe with the history of the evaluations.
        - List with the values of the running loss at each step.
        - The model id.
    """
    train_history: dict[str, list] = {
        'steps': [],
        'time': [],
        'rewards': [],
        'lr': []
    }
    evaluations: List[list] = []
    evaluations_idcs: List[int] = []
    running_loss: List[float] = []
    eval_columns = [
        'rewards_median', 'rewards_mean', 'rewards_std', 'steps_median', 'steps_mean', 'steps_std', 'win_rate',
        'loss_rate', 'draw_rate'
    ]

    scheduler = agent.scheduler

    print(
        f"Training policy in {agent.id}.\n"
        f"{'Episode':^10}{'Step':^10}{'Train rewards (avg)':^20}{'steps (avg)':^14}{'running loss (avg)':^20}"
        f"{'Eval reward (mean std)':^25}{'win rate(%)':^10}{'Eps':^10}{'LR':^6}{'Time':^11}"
    )

    try:
        for episode in range(episodes):
            episode_s = time.time()
            transitions = _play_episode(
                agent, env, sum(train_history['steps']), enforce_valid_action)
            rewards = 0.
            steps = len(transitions)

            for i in range(len(transitions)):
                if i == len(transitions) - 2:
                    state, action, next_state, reward = transitions[i]
                    if transitions[-1].reward == env.rewards['win']:
                        transition = Transition(
                            state=state,
                            action=action,
                            next_state=next_state,
                            reward=reward + env.rewards['loss']
                        )

                    else:
                        transition = Transition(
                            state=state,
                            action=action,
                            next_state=next_state,
                            reward=reward + env.rewards['draw']
                        )

                else:
                    transition = transitions[i]

                agent.cache(*transition)
                rewards += transition.reward

            if len(agent.memory) >= batch_size:
                _optimize(
                    agent=agent,
                    running_loss=running_loss,
                    episode=episode
                )

            train_history['steps'].append(steps)
            train_history['rewards'].append(rewards)
            train_history['time'].append(time.time() - episode_s)
            train_history['lr'].append(agent.optimizer.param_groups[0]['lr'])

            if (len(agent.memory) >= batch_size) and ((episode+1) % eval_period == 0):
                evaluation = _evaluate(
                    agent=agent,
                    env=env,
                    device=device,
                    episodes=evaluation_episodes,
                    enforce_valid_action=enforce_valid_action,
                )

                if (episode+1) % display_period == 0:
                    _print_metrics(
                        agent=agent,
                        evaluation=evaluation,
                        train_history=train_history,
                        running_loss=running_loss,
                        episode=episode,
                        eval_period=eval_period
                    )
                evaluations.append(evaluation)
                evaluations_idcs.append(episode)

            if episode != 0 and checkpoint_every is not None and (episode + 1) % checkpoint_every == 0 \
                    and not (episode == episodes-1 and save_on_exit == True):
                agent.save(
                    checkpoints_dir_path=checkpoints_dir_path,
                    current_step=sum(train_history['steps'])
                )

            if scheduler is not None and len(agent.memory) >= batch_size:
                scheduler.step()

        if save_on_exit:
            agent.save(
                checkpoints_dir_path=checkpoints_dir_path,
                current_step=sum(train_history['steps'])
            )

        return pd.DataFrame.from_dict(data=train_history), pd.DataFrame(evaluations, columns=eval_columns, index=evaluations_idcs), \
            running_loss, agent.id

    except KeyboardInterrupt:
        if save_on_exit:
            agent.save(
                checkpoints_dir_path=checkpoints_dir_path,
                current_step=sum(train_history['steps'])
            )

        return pd.DataFrame.from_dict(data=train_history), pd.DataFrame(evaluations, columns=eval_columns, index=evaluations_idcs), \
            running_loss, agent.id



def plot(train_history: pd.DataFrame, eval_history: pd.DataFrame, running_loss: list, model_id: str, figures_dir_path: str | None = None) -> None:
    """
    Plots training and evaluation metrics.

    Args:
        - `train_history`: Pandas dataframe with the history of the training.
        - `eval_history`: Pandas dataframe with the history of the evaluations.
        - `running_loss`: list with all the running losses.
        - `model_id`: id of the model.
        - `figures_dir_path`: path to the figures directory.
    """
    num_episodes = train_history.shape[0]
    _, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(
        3, 2, sharey=False, figsize=(20, 10), layout='tight')
    running_loss_min = min(running_loss)
    running_loss_max = max(running_loss)

    ax1.set(title="Evaluation rewards", xlabel="Episode", ylabel="Reward",
            xlim=(0, num_episodes))
    eval_history[['rewards_median']].plot(
        ax=ax1, linestyle='solid', c='#669944')
    eval_history[['rewards_mean']].plot(
        ax=ax1, linestyle='solid', c='#003060')
    ax1.fill_between(
        eval_history.index,
        (eval_history[['rewards_mean']].to_numpy() -
         eval_history[['rewards_std']].to_numpy()).flatten(),
        (eval_history[['rewards_mean']].to_numpy() +
         eval_history[['rewards_std']].to_numpy()).flatten(),
        alpha=0.1,
        color='#003060',
        linestyle='dashed'
    )
    ax1.legend(('Mean', 'Median', 'Standard deviation'),
               loc="lower center")

    ax2.set(title="Evaluation steps", xlabel="Episode", ylabel="Step",
            xlim=(0, num_episodes))
    eval_history[['steps_median']].plot(
        ax=ax2, linestyle='solid', c='#669944')
    eval_history[['steps_mean']].plot(
        ax=ax2, linestyle='solid', c='#003060')
    ax2.fill_between(
        eval_history.index,
        (eval_history[['steps_mean']].to_numpy(
        )-eval_history[['steps_std']].to_numpy()).flatten(),
        (eval_history[['steps_mean']].to_numpy(
        )+eval_history[['steps_std']].to_numpy()).flatten(),
        alpha=0.1,
        color='#003060',
        linestyle='dashed'
    )
    ax2.legend(('Mean', 'Median', 'Standard deviation'),
               loc="lower center")

    ax3.set(title="Evaluation rates", xlabel="Episode", ylabel="Percentage",
            ylim=(0, 100), xlim=(0, num_episodes))
    eval_history['win_rate'].map(
        lambda x: x*100).plot(ax=ax3, c='#003060')
    eval_history['loss_rate'].map(
        lambda x: x*100).plot(ax=ax3, c='#c0304b')
    eval_history['draw_rate'].map(
        lambda x: x*100).plot(ax=ax3, c='#669944')
    ax3.legend(('Win', 'Loss', 'Draw'),
               loc="lower center")

    ax4_y_lower_bound = running_loss_min - \
        (0.1*(running_loss_max - running_loss_min))
    ax4_y_upper_bound = running_loss_max + \
        (0.1*(running_loss_max - running_loss_min))
    ax4.set(
        title="Running loss",
        xlabel="Step",
        ylabel="Loss",
        ylim=(ax4_y_lower_bound, ax4_y_upper_bound),
        xlim=(0, len(running_loss))
    )
    ax4.plot(
        running_loss,
        c='#003060')

    ax5.set(
        title="Training rewards",
        xlabel="Episode",
        ylabel="Reward",
        xlim=(0, num_episodes)
    )
    ax5.plot(
        train_history['rewards'],
        c='#003060'
    )
    ax5.plot(
        moving_average(train_history['rewards'].to_numpy()[
                       :num_episodes+1], 50),
        c='#47a2ff'
    )
    ax5.legend(
        ('Running', 'Average'),
        loc="lower center"
    )

    ax6.set(
        title="Training steps",
        xlabel="Episode",
        ylabel="Step",
        xlim=(0, num_episodes)
    )
    ax6.plot(
        train_history['steps'], c='#003060')
    ax6.plot(moving_average(train_history['steps'].to_numpy()[
        :num_episodes+1], 50), c='#47a2ff')
    ax6.legend(('Running', 'Average'), loc="upper center")

    if figures_dir_path is not None:
        Path(figures_dir_path).mkdir(
            parents=True,
            exist_ok=True
        )
        figure_path = join(figures_dir_path, f'{model_id}.png')
        plt.savefig(fname=figure_path)
        print(f"Model training and evaluation figure saved to {figure_path}")

    plt.show()


def _play_episode(agent: Agent, env: ConnectXEnv, total_steps: int, enforce_valid_action: bool) -> List[Transition]:
    """
    Plays an entire episode and return a list with all the transitions unaltered.

    Args:
        - `agent`: agent of type `Agent`.
        - `env`: environment of type `ConnectXEnv`.
        - `total_steps`: total number of steps taken during the training so far.
        - `enforce_valid_action`: whether to enforce that the action is valid or not.

    Returns:
        - A list with all the transitions unaltered.
    """
    agent.networks.policy.train()
    transitions: List[Transition] = []
    # Initialize the environment and get its state
    state = env.reset()
    while True:
        num_steps = total_steps + len(transitions)

        valid_actions = env.get_valid_actions()

        action = agent.act(
            state=state,
            valid_actions=valid_actions,
            num_steps=num_steps,
            enforce_valid_action=enforce_valid_action
        )

        if action not in valid_actions:
            reward = env.rewards['loss']
            transition = Transition(
                state=state,
                action=action,
                next_state=None,
                reward=reward)
            transitions.append(transition)
            break

        next_state, reward, is_done = env.step(action)
        transition = Transition(
            state=state,
            action=action,
            next_state=next_state,
            reward=reward
        )
        transitions.append(transition)

        if is_done:
            break

        state = next_state

        env.switch_turn()

    return transitions


def _optimize(agent: Agent, running_loss: list, episode: int) -> None:
    """
    Optimizes the policy in `agent`, updates the target in `agent` and appends the loss
    to `running_loss`.

    Args:
        - `agent`: agent of type `Agent`.
        - `running_loss`: list with the running losses.
        - `episode`: current episode of the training.
    """
    loss = agent.optimize()
    running_loss.append(loss)
    agent.update_target(episode)


def _evaluate(agent: Agent, env: ConnectXEnv, device: torch.device, episodes: int, enforce_valid_action: bool) -> list:
    """
    Evaluates policy in `agent` against a random agent in both turns, i.e. both strategies.

    Args:
        - `agent`: agent of type `Agent`.
        - `env`: environment of type `ConnectXEnv`.
        - `device`: torch device.
        - `episodes`: the number of episodes
        - `enforce_valid_action`: whether to enforce that the action is valid or not.

    Returns:
        - A list with the evaluation metrics.
    """
    agent.networks.policy.eval()
    episodes_rewards = []
    episodes_steps = []
    rates = np.zeros(episodes, dtype=np.int8)
    for episode in range(0, episodes):
        player = np.random.choice([1, 2])
        state = env.reset()
        episode_rewards = 0.
        episode_steps = 0
        while True:
            valid_actions = env.get_valid_actions()
            if player == env.turn:
                action = agent.exploit(
                    state, valid_actions, enforce_valid_action)
                if not enforce_valid_action and action not in valid_actions:
                    episode_rewards += env.rewards['loss']
                    episodes_rewards.append(episode_rewards)
                    episodes_steps.append(episode_steps)
                    rates[episode] = -1
                    break

            else:
                action = np.random.choice(valid_actions)

            next_state, reward, is_done = env.step(action)
            episode_steps += 1
            if is_done:
                if env.winner == player:
                    episode_rewards += reward

                episodes_rewards.append(episode_rewards)
                episodes_steps.append(episode_steps)
                rates[episode] = 0 if env.winner is None else 1 if env.winner == player else -1
                break

            if env.turn == player:
                episode_rewards += reward

            state = next_state
            env.switch_turn()

    values, counts = np.unique(rates, return_counts=True)
    value_counts = dict(zip(values, counts))
    win_rate = value_counts[1] / \
        len(rates) if 1 in value_counts else 0
    draw_rate = value_counts[0] / len(rates) if 0 in value_counts else 0
    loss_rate = 1 - win_rate - draw_rate

    evaluation = [np.median(episodes_rewards), np.mean(episodes_rewards), np.std(episodes_rewards), np.median(
        episodes_steps),  np.mean(episodes_steps), np.std(episodes_steps),
        win_rate, loss_rate, draw_rate]

    agent.networks.policy.train()
    return evaluation


def _print_metrics(agent: Agent, evaluation: list, train_history: dict[str, list], running_loss: list, episode: int, eval_period:int) -> None:
    """
    Prints evaluation and training metrics.

    Args:
        - `agent`: agent of type `Agent`.
        - `evaluation`: list with the evaluation metrics.
        - `train_history`: dictionary with the training metrics.
        - `params_eval`: `ParamsEval` object with the parameters of the evaluations.
        - `running_loss`: list with the running losses.
        - `episode`: current episode of the training.
        - `eval_period`: period for the evaluation.
    """
    minutes, seconds = divmod(
        train_history['time'][episode-1], 60)
    duration = f'{int(minutes)}m {round(seconds, 2)}s' if minutes > 0 else f'{round(seconds, 2)}s'
    running_loss_window = len(running_loss) if len(
        running_loss) < eval_period else eval_period
    train_history_window = len(train_history['rewards']) if len(
        train_history['rewards']) < eval_period else eval_period
    train_rewards_str = f"{round(train_history['rewards'][-1], 4)} ({round(moving_average(train_history['rewards'], train_history_window)[-1], 4)})"
    train_steps_str = f"{round(train_history['steps'][-1], 4)} ({round(moving_average(train_history['steps'], train_history_window)[-1], 4)})"
    running_loss_str = f"{round(running_loss[-1], 4)} ({round(moving_average(running_loss, running_loss_window)[-1], 4)})"
    eval_reward = f"{np.round(evaluation[1], 4)}  {np.round(evaluation[2], 4)}"

    print(
        f"{episode:^10}{sum(train_history['steps']):^10}{train_rewards_str:^20}{train_steps_str:^14}{running_loss_str:^20}{eval_reward:^25}"
        f"{round(evaluation[6]*100, 2):^10}{str(round(agent.eps_threshold, 4)):^10}{str(round(agent.optimizer.param_groups[0]['lr'], 8)):^6}{duration:^11}"
    )
