import torch
import torch.nn as nn
import torch.nn.functional as F
import copy
from typing import Literal, List


class Module(nn.Module):
    def layer_summary(self, x_shape: List[int]) -> None:
        """
        Prints a summary of all the Conv2d, Flatten and Linear layers and their outputs to
        a Tensor with shape `x_shape`.

        Args:
            - `x_shape`: shape of the tensor used as input to the layers.
        """
        x = torch.randn(*x_shape)
        for module in self.policy.modules():
            if isinstance(module, nn.Conv2d) or isinstance(module, nn.Flatten) or isinstance(module, nn.Linear):
                x = module(x)
                title = f'{module.__class__.__name__} output shape:'
                print(f'{title:30}{x.shape}')


class ConnectXNet(Module):
    def __init__(self, out_features: int):
        super(ConnectXNet, self).__init__()
        self.out_features = out_features

        self.policy = nn.Sequential(
            # (N, Cin, Hin, Win) -> (N, Cout, Hout, Wout)
            nn.Conv2d(
                in_channels=2,
                out_channels=16,
                kernel_size=3,
                stride=1,
                padding=1
            ),
            nn.ReLU(),
            # (N, Cin, Hin, Win) -> (N, Cout, Hout, Wout)
            nn.Conv2d(
                in_channels=16,
                out_channels=16,
                kernel_size=3,
                stride=1,
                padding=1
            ),
            nn.ReLU(),
            # (N, Cin, Hin, Win) -> (N, Hout)
            nn.Flatten(),
            # (N, Win) -> (N, Hout)
            nn.Linear(
                in_features=672,
                out_features=128
            ),
            nn.ReLU(),
            # (N, Win) -> (N, Hout)
            nn.Linear(
                in_features=128,
                out_features=out_features
            )
        )

        for module in self.modules():
            if isinstance(module, nn.Conv2d) | isinstance(module, nn.Linear):
                if isinstance(module.weight.data, torch.Tensor):
                    nn.init.xavier_uniform_(module.weight.data)
                module.bias.data.zero_

        self.target = copy.deepcopy(self.policy)
        for param in self.target.parameters():
            param.requires_grad = False

    def forward(self, x: torch.Tensor, model: Literal['policy', 'target']):
        if model == 'policy':
            return self.policy(x)

        elif model == 'target':
            return self.target(x)
