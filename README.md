# Game Engines

## About

This repo is a personal project made to learn deep learning reinforcement techniques. It contains copies and reworks
of code taken from other public repos.

## Connect 4
The first usecase is the training of a Deep-Q Network that plays the Connect 4 game. It's a relatively well
documented subject and a good first step to start learning. 

One of the major source for this work is from:
[zk-connect-four on Github](https://github.com/albertobas/zk-connect-four)


## Licence

This repo is under GPL 3.0 Licence to comply with its different sources.