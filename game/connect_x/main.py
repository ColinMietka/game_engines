import torch

from training import train, plot
from env import ConnectXEnv
from agent import Agent
from network import ConnectXNet

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# networks
networks = ConnectXNet(out_features=7).to(device)

# agent
load_model_path = None
# load_model_path = join(EXPORTS_DIR_PATH, 'checkpoints', 'ConnectFourNet_2023-10-10T07:52:28.chkpt')

agent = Agent(
    networks=networks,
    device=device,
    load_model_path=load_model_path
)

# environment
env = ConnectXEnv(
    device=device
)

# train
args = train(
    agent=agent,
    env=env,
    checkpoints_dir_path='../../models',
    device=device
)

# plot
plot(
    *args,
    figures_dir_path="../../figures",
    )