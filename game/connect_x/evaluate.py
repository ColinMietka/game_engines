import torch
from agent import Agent
from env import ConnectXEnv
from network import ConnectXNet
import random
import collections
from tqdm import tqdm

device = torch.device('cpu')


def play_game(device, agent_starts=True):

    game = ConnectXEnv(
        device=device
    )
    if agent_starts:
        game.turn = 2

    while not game.is_done:
        # In a real implementation, you would have an AI making decisions here
        available_columns = game.get_valid_actions()

        if game.turn == 1:
            current_player = game.players[game.turn -1]
            column = random.choice(available_columns)
            game.step(int(column))
            game.switch_turn()
        else:
            column = agent.exploit(state=game.board, valid_actions=available_columns, enforce_valid_action=True)
            game.step(int(column))
            game.switch_turn()

    # print('Winner:', game.players[game.winner-1])
    return game.winner

if __name__ == '__main__':

    # networks
    networks = ConnectXNet(out_features=7).to(device)

    # agent
    load_model_path = '../../models/DQN_2025_02_11_T_10_56_16.chkpt'

    agent = Agent(
        networks=networks,
        device=device,
        load_model_path=load_model_path
    )

    episodes = 10000
    winners = []
    for episode in tqdm(range(episodes)):
        if episode % 2 == 0:
            winners.append(play_game(device))
        else:
            winners.append(play_game(device, agent_starts=False))


    print(collections.Counter(winners))