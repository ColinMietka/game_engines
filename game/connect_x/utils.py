import numpy as np

def get_two_channels(observation: np.ndarray) -> np.ndarray:
    """
    Applies a transformation on the observation to obtain an array with binary values for each
    player from an array with values 0, 1 or 2.

    Args:
    - `observation`: observation of the environment.

    Returns:
        - Array with both dimensions stacked.
    """
    p1_batch = observation.copy()
    p2_batch = observation.copy()
    p1_batch[p1_batch == 2] = 0
    p2_batch[p2_batch == 1] = 0
    p2_batch[p2_batch == 2] = 1
    return np.stack(
        arrays=(p1_batch, p2_batch),
        axis=1
    )


def moving_average(x: list | np.ndarray, n: int) -> np.ndarray:
    """
    Calculate moving average of `x` with a window of `n`.

    Args:
        - `x`: list or array with the values.
        - `n`: window of the moving average.

    Returns:
        - Array with the averaged values.
    """
    cum_sum = np.cumsum(np.insert(x, 0, 0))
    return (cum_sum[n:] - cum_sum[:-n]) / float(n)