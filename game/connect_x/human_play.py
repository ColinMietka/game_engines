import torch
from agent import Agent
from env import ConnectXEnv
from network import ConnectXNet

device = torch.device('cpu')


if __name__ == '__main__':

    # networks
    networks = ConnectXNet(out_features=7).to(device)

    # agent
    load_model_path = '../../models/DQN_2025_02_11_T_10_56_16.chkpt'

    agent = Agent(
        networks=networks,
        device=device,
        load_model_path=load_model_path
    )

    # environment
    game = ConnectXEnv(
        device=device
    )

    while not game.is_done:
        # In a real implementation, you would have an AI making decisions here
        available_columns = game.get_valid_actions()

        if game.turn == 1:
            game.visualize()
            current_player = game.players[game.turn -1]
            column = input(f"You play first ({current_player}). Enter column number (0-{len(available_columns) - 1})")

            if column.isdigit() and int(column) in available_columns:
                game.step(int(column))
                game.switch_turn()
            else:
                print("Invalid move. Try again.")
        else:
            column = agent.exploit(state=game.board, valid_actions=available_columns, enforce_valid_action=True)
            game.step(int(column))
            game.switch_turn()


    print(f"And the winner is Player {game.players[game.winner -1]} !")
    game.visualize()
