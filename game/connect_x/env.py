import torch
import numpy as np
from typing import List

class ConnectXEnv:
    """
    Class representing the ConnectX board states and updates
    """
    def __init__(self, device: torch.device, connect: int=4, action_space: int=7, observation_space: int=6):
        self.device = device
        self.connect = connect
        self.observation_space = observation_space
        self.action_space = action_space
        self.board: np.ndarray = np.zeros(
            shape=[self.observation_space, self.action_space],
            dtype=np.int8
        )
        self.is_done = False
        self.turn: int = 1
        self.players = ['X', 'O']
        self.winner: None | int = None
        self.rewards = {
            'win': 1.,
            'loss': -1.,
            'draw': 0.,
            'prolongation': -0.05
        }

    def get_valid_actions(self) -> List[int]:
        """
        Returns a list with all the valid actions for the current board
        """
        valid_actions = []
        for col in range(self.action_space):
            for row in range(0, len(self.board)):
                if self.board[row][col] == 0:
                    valid_actions.append(col)
                    break
        return valid_actions

    def step(self, action: int) -> tuple[np.ndarray, float, float]:
        """
        Edits the state (it adds a new counter to the board), checks for a winner and for a draw and
        returns the state and the reward.

        Args:
            - `action`: a valid column index

        Returns:
            - A tuple of numpy arrays with the new state and the reward
        """
        row_index = np.sum([self.board[:, action] == 0]) - 1
        self.board[row_index, action] = self.turn
        self.check_game_over()
        reward = self._get_reward()
        return self.board.copy(), reward, float(self.is_done)

    def reset(self) -> np.ndarray:
        """
        Sets board, is_done flag, turn and winner to their initial state
        """
        self.board = np.zeros(
            shape=[self.observation_space,
                   self.action_space],
            dtype=np.int8
        )
        self.is_done = False
        self.turn = 1
        self.winner = None

        return self.board.copy()

    def switch_turn(self) -> None:
        """
        Switches the turn.
        """
        if self.turn == 1:
            self.turn = 2
        else:
            self.turn = 1

    def visualize(self):
        """
        Displays a visual representation of the board to be able to play
        """
        display_board = [[' ' for _ in range(self.action_space)] for _ in range(self.observation_space)]

        for x in range(self.observation_space):
            for y in range(self.action_space):
                piece = self.board[x][y]
                if piece > 0:
                    display_board[x][y] = self.players[piece - 1]


        print("\nCurrent Board State:")
        print("--------------------")
        for row in range(self.observation_space):
            print(f"| {' | '.join(display_board[row])} |")
        if not self.is_done:
            print(f"\nPlayer {self.players[self.turn -1]}'s turn!")

    def check_game_over(self):
        """
        Checks if the game is over and assign winner if there is one
        """

        directions = [
            (0, 1),  # Horizontal
            (1, 0),  # Vertical
            (1, 1),  # Diagonal down-right
            (1, -1)  # Diagonal down-left
        ]

        for x in range(self.observation_space):
            for y in range(self.action_space):

                if self.board[x][y] != 0:
                    for dx, dy in directions:
                        count = 1
                        r, c = x + dx, y + dy

                        while 0 <= r < self.observation_space and 0 <= c < self.action_space:
                            if self.board[r][c] == self.board[x][y]:
                                count += 1
                                r += dx
                                c += dy
                            else:
                                break

                        if count >= self.connect:
                            self.is_done = True
                            self.winner = self.turn

        # check if draw when there are no moves remaining
        if len(self.get_valid_actions()) == 0:
            self._check_draw()

    def _check_legal_action(self, move):
        """
        Checks if move is inside the board
        :param move: [x,y] position on board
        :return: Boolean
        """
        x = move[0]
        y = move[1]
        condition = (0 <= x <= (self.observation_space-1)) and (0 <= y <= (self.action_space-1))
        return condition

    def _get_reward(self) -> float:
        """
        Returns the obtained reward. It only penalizes prolongations after eight total moves.

        Returns:
            - The reward.
        """
        if self.is_done:
            if self.winner is None:  # if draw
                return self.rewards['draw']
            else:  # if win
                return self.rewards['win']
        else:  # do not penalize if players have not played at least 4 times
            return self.rewards['prolongation'] if np.sum([self.board != 0]) > 8 else 0.

    def _check_draw(self) -> None:
        """
        Checks if the board is full, i.e., if the state does not contain any zero.
        If it is, it sets `is_done` to True.
        """
        if (self.winner is None) & (np.sum([self.board == 0]) == 0):
            self.is_done = True


if __name__ == '__main__':

    game = ConnectXEnv(device=torch.device('cpu'))

    while not game.is_done:
        # In a real implementation, you would have an AI making decisions here
        available_columns = game.get_valid_actions()
        game.visualize()
        current_player = game.players[game.turn -1]
        column = input(f"Enter column number (0-{len(available_columns) - 1}) for Player {current_player}: ")

        if column.isdigit() and int(column) in available_columns:
            game.step(int(column))
            game.switch_turn()
        else:
            print("Invalid move. Try again.")

    print(f"And the winner is Player {game.players[game.winner -1]} !")
    game.visualize()
